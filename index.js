
// let mobile_nav = document.querySelector(".icons-div");
// const header = document.querySelector(".header")
// const toogleBtn = ()=>{
//     // alert('kkkk')
//     header.classList.toggle("active")
// }

// mobile_nav.addEventListener('click',()=>toogleBtn())


const getData = async () => {
    const response = await fetch('https://restcountries.com/v3.1/all');
    const data = await response.json();
    // console.table(data)

    for (let d of data) {
        // console.log(d.flags.png)
        let t = d.timezones[0]
        let dateandtime = getDate(t)
        const cr = d.currencies;
        let crr = Object.values(cr)[0].name

        cardView(d.name.common, d.flags.png, crr, d.maps.googleMaps, d.cioc,dateandtime)
    }
}
getData()


const data = [1, 2, 3, 4]


// for (let i of data) {
    let card_div = document.querySelector('.card-div')
   
   
function cardView(name, flag_url, currencies, mapUrl, code,dateandtime) {
    let container_list = document.createElement('div')
    container_list.classList.add('container-list')
    let img = document.createElement('img')
    img.src = flag_url

    let right_div = document.createElement('div');
    right_div.classList.add('right-div')

    let contryName = document.createElement('p')
    contryName.classList.add('heading')
    let currency = document.createElement('p')
    let dateTime = document.createElement('p')
    currency.classList.add('cr_date')
    dateTime.classList.add('cr_date')
    dateTime.textContent = "Current date Time : "+dateandtime;

    let divbtn = document.createElement('div')
    divbtn.classList.add('divbtn')
    let mapBtn = document.createElement('a')
    let detailBtn = document.createElement('a');

    mapBtn.href = mapUrl
    detailBtn.href = 'details.html?code=' + code
    mapBtn.target = '_blank'


    contryName.textContent = name
    currency.textContent = "currency : " + currencies
    

    mapBtn.textContent = 'Show Map'
    detailBtn.textContent = "Detail"

    divbtn.appendChild(mapBtn)
    divbtn.appendChild(detailBtn)

    right_div.appendChild(contryName)
    right_div.appendChild(currency)
    right_div.appendChild(dateTime)

    container_list.appendChild(img)
    container_list.appendChild(right_div)
    right_div.appendChild(divbtn)
    card_div.appendChild(container_list)
    // }
}

const getDate = (timezone) => {

    timezone = timezone.split('UTC')
       
        

    const d = new Date();
    const localTime = d.getTime();
    const localOffset = d.getTimezoneOffset() * 60000;

    const utc = localTime + localOffset;
    const offset = timezone[1].split(':')[0]; // UTC of Dubai is +04.00
    const dubai = utc + (3600000 * offset);

    const timeNow = new Date(dubai).toLocaleString();
    // console.log(timeNow)
    // console.log(timezone[1].split(':')[0])
    return timeNow
}

function myFunction() {
    
    // Declare variables
    var input, filter, ul, li, a, i, txtValue;
    let cardDiv;
    input = document.getElementById('myInput');
    filter = input.value.toUpperCase();
    cardDiv = document.getElementById("card-div");
    let div = cardDiv.getElementsByTagName('div');

    // let p = div[0].getElementsByTagName("p")[0];
    // console.log(div[8])
    for (i = 0; i < div.length; i+=3) {
      
      let p = div[i].getElementsByTagName("p")[0];
      txtValue = p.textContent || p.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        div[i].style.display = "";
      } else {
        div[i].style.display = "none";
      }
      
    }
  }
