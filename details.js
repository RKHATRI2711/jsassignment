
const getDetails = async(code)=>{
    const res = await fetch('https://restcountries.com/v3.1/alpha/'+code);
    const data = await res.json();
    // console.table(data)
    return await data;
}

function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}


const getDataAll = async()=>{
    const response = await fetch('https://restcountries.com/v3.1/all');
    const data = await response.json();
    return data;
}

const getBoders = async(borders)=>{
    let nearContry = []
    await getDataAll().then((d)=>{
        borders.forEach(element => {
            const obj = d.filter((item)=>{
                return item.cca3 == element
            })
            nearContry.push(obj[0].flags.png)
        });
    })
    console.log(nearContry)

    return nearContry;

}